import React from "react";
import { scaleRotate as Menu } from "react-burger-menu";
import {NavLink} from "react-router-dom";

export default props => {
  return (
    // Pass on our props
    <Menu {...props}>
      <NavLink exact to="/" className="menu-item" >Acceuil</NavLink>
      <NavLink exact to="/login" className="menu-item" >Inscription/Connexion</NavLink>
    </Menu>
  );
};
