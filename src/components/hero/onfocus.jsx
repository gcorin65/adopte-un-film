import React, { Component } from "react";
import Slider from "react-slick";
import BlockRevealAnimation from 'react-block-reveal-animation';


export default class Onfocus extends Component {
  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,      
    };
    return (
      <div>
        <Slider {...settings}>
          <div className="focus focus-item-1">
            <BlockRevealAnimation delay={0.5} color="#9b1313" className="focus-title">
                Creed II
            </BlockRevealAnimation>
          </div>
          <div className="focus focus-item-2">
            <BlockRevealAnimation delay={5} color="#9b1313" className="focus-title">
                Aquaman
            </BlockRevealAnimation>
          </div>
          <div className="focus focus-item-3" >
            <BlockRevealAnimation delay={9} color="#9b1313" className="focus-title">
                Bumblebee
            </BlockRevealAnimation>
          </div>
        </Slider>
      </div>
    );
  }
}