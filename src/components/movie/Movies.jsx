import React, { Component } from 'react';
import './card.scss'



export default class Movies extends Component {
  // view movie details url
  viewMovie() {
    const url = "https://www.themoviedb.org/movie/" + this.props.movie.id
    window.location.href = url
  }
  render() {
    return  (
      <div className='card'>
        <div className='thumbnails'>
          <img width='100%' height='350' src={this.props.movie.poster_src} alt="Poster about movie"/>
        </div>
        <div className='content_card'>
          <h3 onClick={this.viewMovie.bind(this)}>{this.props.movie.title}</h3>
          <p>{this.props.movie.overview}</p>
          <input type="button" onClick={this.viewMovie.bind(this)} value="Fiche"/>
        </div>
      </div>

    );
  }
}

