import React, { Component } from 'react';
import SideBar from "./components/sidebar/Sidebar.jsx";
import Onfocus from './components/hero/Onfocus';
import Movies from './components/movie/Movies';
import {Grid,Row,Col} from 'react-bootstrap';
import $ from 'jquery'

import './scss/wrap.scss'
import './scss/onfocus.scss'

class App extends Component {
  constructor(props){
    super(props)
    this.state ={}
    this.performSearch("creed")
  }
  performSearch(searchTerm) {
    const urlString = "https://api.themoviedb.org/3/search/movie?api_key=1b5adf76a72a13bad99b8fc0c68cb085&query=" + searchTerm
    $.ajax({
      url: urlString,
      success: (searchResults) => {
        console.log("Fetched data successfully")
        const results = searchResults.results

        var movieRows = [] 
        results.forEach((movie) => {
          movie.poster_src = "https://image.tmdb.org/t/p/original" + movie.poster_path
          const movieRow = <Movies key={movie.id} movie={movie}/>
          movieRows.push(<Col xs={12} md={4} lg={4} key={movie.id}>{movieRow}</Col>)
        })

        this.setState({rows: movieRows})
      },
      error: (xhr, status, err) => {
        console.error("Failed to fetch data")
      }
    })
  }
  // Function searchTerm for searchBar
  searchChangeHandler(event) {
    console.log(event.target.value)
    const boundObject = this
    const searchTerm = event.target.value
    boundObject.performSearch(searchTerm)
  }
  render() {
    return (
      <div id="App">
        <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
        <div id="page-wrap">
          <Grid>
          <Row>
            <Col xs={1} md={1} style={{paddingTop:15, paddingLeft:40}}>
              <img width='50'  src="movie.jpg" alt="movie app icon"/>
            </Col>
            <Col xs={11} md={11} style={{paddingBottom:10}}>
              <h1>Adopt a movie</h1>
            </Col>
          </Row>
          </Grid>          
          <Onfocus></Onfocus>
          <Grid>
            <div className="top-week">
              <h2>Liste des films disponibles</h2>
              <input style={{
                fontSize:24,
                display:'block',
                paddingLeft:10,
                marginTop:20,
                marginBottom:50  
              }}onChange={this.searchChangeHandler.bind(this)} placeholder="Rechercher un film"/>
            </div>
            {this.state.rows}
          </Grid>
          <div className="footer" style={{
          width:"100%",
          height:100,
          backgroundColor:"#9b1313",
          marginTop:50
        }}>
        <p style={{
           textAlign:"center",
           paddingTop:45
        }}> <a style={{
            color: "#ffffff",
            fontSize:15,
          }} href="https://www.codewars.com/users/Gregcrn/completed_solutions">CodeWars: Wich are in?</a></p>
         
        </div>
        </div>
      </div>
      
    );
  }
}

export default App;
